import logo from "./logo.svg";
import "./App.css";
import BaiTap1 from "./BaiTap1/BaiTap1";

function App() {
  return (
    <div className="App">
      <BaiTap1 />
    </div>
  );
}

export default App;
